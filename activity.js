let http = require("http");

http.createServer(function(request,response){

	console.log(request.url);
	console.log(request.method);

	//	1. Welcome
	if(request.url === "/" && request.method === "GET"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Welcome to Booking System");

	// 2. Profile
	} else if (request.url === "/profile" && request.method === "GET"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Welcome to your profile!");

	// 3. Here is our courses
	} else if(request.url === "/courses" && request.method === "GET"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Here's our courses available");

	} else if(request.url === "/addcourse" && request.method === "POST"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Add course to our resources");
		
	} else if(request.url === "/updatecourse" && request.method === "PUT"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Update a course to our resources");

	// 2. Delete
	} else if (request.url === "/archivecourses" && request.method === "DELETE"){

		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Archive courses to our resources");
	}
}).listen(4000);

console.log("Server running at localhost:4000");